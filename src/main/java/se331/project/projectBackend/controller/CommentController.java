package se331.project.projectBackend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import se331.project.projectBackend.entity.Activity;
import se331.project.projectBackend.entity.Comment;
import se331.project.projectBackend.security.entity.User;
import se331.project.projectBackend.security.repository.UserRepository;
import se331.project.projectBackend.service.ActivityService;
import se331.project.projectBackend.service.CommentService;
import se331.project.projectBackend.util.MapperUtil;

@CrossOrigin
@Controller
public class CommentController {
    @Autowired
    CommentService commentService;
    @Autowired
    ActivityService activityService;
    @GetMapping("/comments")
    public ResponseEntity getAllComments() {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getCommentDto(commentService.getAllComments()));
    }
    @GetMapping("/comments/{id}")
    public ResponseEntity getCommentById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getCommentDto(commentService.findById(id)));
    }
    @DeleteMapping("/comments/{id}")
    public ResponseEntity<?> deleteCommentById(@PathVariable long id) {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getCommentDto(commentService.deleteById(id)));
    }
    @Autowired
    UserRepository userService;
    @CrossOrigin
    @PostMapping("/comments")
    public ResponseEntity saveComment(@RequestBody Comment comment) {
        Long actualUserId = userService.findByAppUserId(comment.getPoster().getId()).getId();
        comment.setPoster(new User());
        comment.getPoster().setId(actualUserId);
        comment = commentService.saveComment(comment);
        Activity activity = activityService.findById(comment.getActivity().getId());
        activity.getComments().add(comment);
        activityService.saveActivity(activity);
        return ResponseEntity.ok(MapperUtil.INSTANCE.getCommentDto(comment));
      
    }

}
