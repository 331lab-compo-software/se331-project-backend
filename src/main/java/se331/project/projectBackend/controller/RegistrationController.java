package se331.project.projectBackend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import se331.project.projectBackend.dto.StudentUserDto;
import se331.project.projectBackend.entity.Student;
import se331.project.projectBackend.security.entity.Authority;
import se331.project.projectBackend.security.entity.AuthorityName;
import se331.project.projectBackend.security.entity.User;
import se331.project.projectBackend.security.repository.AuthorityRepository;
import se331.project.projectBackend.security.repository.UserRepository;
import se331.project.projectBackend.service.StudentService;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

@Controller
public class RegistrationController {
    @Autowired
    StudentService studentService;
    @Autowired
    UserRepository userRepository;
    PasswordEncoder encoder = new BCryptPasswordEncoder();
    @Autowired
    AuthorityRepository authorityRepository;
    @CrossOrigin
    @PostMapping("/registerStudent")
    public ResponseEntity<?> saveUser(@RequestBody StudentUserDto studentUserDto) {
        Authority studentAuthority = authorityRepository.findByName(AuthorityName.ROLE_STUDENT);
        List<Authority> authorities = new ArrayList<>();
        authorities.add(studentAuthority);
        User newUser = User.builder()
                .username(studentUserDto.getEmail())
                .firstname(studentUserDto.getName())
                .lastname(studentUserDto.getSurname())
                .password(encoder.encode(studentUserDto.getPassword()))
                .email(studentUserDto.getEmail())
                .enabled(true)
                .authorities(authorities)
                .lastPasswordResetDate(Date.from(LocalDate.of(2019, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
                .build();
        newUser = userRepository.save(newUser);
        Student newStudent = Student.builder()
                .id(newUser.getId())
                .studentId(studentUserDto.getStudentId())
                .name(studentUserDto.getName())
                .surname(studentUserDto.getSurname())
                .image(studentUserDto.getImage())
                .dob(studentUserDto.getDob())
                .approved(false)
                .user(newUser)
                .build();
        newUser.setAppUser(newStudent);
        newStudent = this.studentService.saveStudent(newStudent);
        newUser = userRepository.save(newUser);
        return ResponseEntity.ok(StudentUserDto.builder()
                .id(newUser.getId())
                .username(newUser.getUsername())
                .email(newUser.getEmail())
                .studentId(newStudent.getStudentId())
                .name(newStudent.getName())
                .surname(newStudent.getSurname())
                .approved(false)
                .image(newStudent.getImage())
                .dob(newStudent.getDob())
                .build());
    }
    @CrossOrigin
    @GetMapping("/studentUsers")
    public ResponseEntity<?> getStudentUsers() {
        List<Student> students = studentService.getAllStudent();
        List<StudentUserDto> studentUserDtoList = new ArrayList<>();
        for (Student s: students) {
            User user = s.getUser();
            StudentUserDto studentUserDto = StudentUserDto.builder()
                    .id(s.getId())
                    .studentId(s.getStudentId())
                    .email(user.getEmail())
                    .approved(s.isApproved())
                    .name(s.getName())
                    .surname(s.getSurname())
                    .dob(s.getDob())
                    .image(s.getImage())
                    .username(user.getUsername())
                    .password(user.getPassword())
                    //Prevent overflow (More like we are lazy)
                    //.user(s.getUser())
                    .lastPasswordResetDate(user.getLastPasswordResetDate())
                    .authorities(user.getAuthorities())
                    .build();
            studentUserDtoList.add(studentUserDto);
        }
        return ResponseEntity.ok(studentUserDtoList);
    }
    @CrossOrigin
    @PutMapping("/studentUsers/{id}/enable")
    public ResponseEntity<?> enableStudent(@PathVariable("id") Long id,@RequestBody StudentUserDto studentUserDto) {
        Student student = studentService.findById(id);
        student.setApproved(true);
        student.getUser().setAppUser(student);
        User tempUser = student.getUser();
        userRepository.save(tempUser);
        studentService.saveStudent(student);
        return ResponseEntity.ok(student);
    }
    @CrossOrigin
    @PutMapping("/studentUsers/{id}/disable")
    public ResponseEntity<?> disableStudent(@PathVariable("id") Long id,@RequestBody StudentUserDto studentUserDto) {
        Student student = studentService.findById(id);
        student.setApproved(false);
        student.getUser().setAppUser(student);
        User tempUser = student.getUser();
        userRepository.save(tempUser);
        studentService.saveStudent(student);
        System.out.println(student.isApproved());
        return ResponseEntity.ok(student);
    }
        @CrossOrigin
        @PostMapping("/registerStudent/checkDupEmail")
        public ResponseEntity<?> checkDupEmail(@RequestBody StudentUserDto studentUserDto) {
            String email = studentUserDto.getEmail();
            Map<String,Boolean> result = new HashMap();

            if (userRepository.findByUsername(email) != null )
                result.put("dup",true);
            else
                result.put("dup",false);
            return ResponseEntity.ok(result);
        }
    @CrossOrigin
    @PostMapping("/registerStudent/checkDupStudentId")
    public ResponseEntity<?> checkDupStudentId(@RequestBody StudentUserDto studentUserDto) {
        String newStudentId = studentUserDto.getStudentId();
        Map<String,Boolean> result = new HashMap();
        List<Student> students = studentService.getAllStudent();
        for (Student s : students) {
            if (s.getStudentId().equals(newStudentId)) {

                result.put("dup",true);
                return ResponseEntity.ok(result);
            }
        }
        result.put("dup",false);
        return ResponseEntity.ok(result);
    }
    @CrossOrigin
    @PostMapping("/verifyPassword/{username}/{password}")
    public ResponseEntity<?>  verifyPassword(@PathVariable String username,@PathVariable String password) {
        Map<String,Boolean> result = new HashMap();
        User user = userRepository.findByUsername(username);
            if (encoder.matches(password,user.getPassword())) {
                result.put("match", true);
                return ResponseEntity.ok(result);
            }
        result.put("match",false);
        return ResponseEntity.ok(result);
    }

    }


