package se331.project.projectBackend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import se331.project.projectBackend.dto.ActivityDto;
import se331.project.projectBackend.dto.StudentUserDto;
import se331.project.projectBackend.entity.Activity;
import se331.project.projectBackend.entity.Lecturer;
import se331.project.projectBackend.entity.Student;
import se331.project.projectBackend.service.ActivityService;
import se331.project.projectBackend.service.LecturerService;
import se331.project.projectBackend.service.StudentService;
import se331.project.projectBackend.util.MapperUtil;

import java.util.ArrayList;
import java.util.List;

@Controller
public class ActivityController {
    @Autowired
    ActivityService activityService;
    @Autowired
    LecturerService lecturerService;
    @Autowired
    StudentService studentService;
    @GetMapping("/activities")
    public ResponseEntity getAllActivities() {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getActivityDto(activityService.getAllActivities()));
    }
    @GetMapping("/activities/{id}")
    public ResponseEntity getActivityById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getActivityDto(activityService.findById(id)));
    }
    @DeleteMapping("/activities/{id}")
    public ResponseEntity<?> deleteActivityById(@PathVariable Long id) {
        return ResponseEntity.ok(activityService.deleteById(id));
    }
    @CrossOrigin
    @PostMapping("/activities")
    public ResponseEntity saveActivity(@RequestBody Activity activity) {
        System.out.println(activity.getHost().getId());
        Lecturer lecturer = lecturerService.findById(activity.getHost().getId());
        System.out.println(lecturer.getId());
        return ResponseEntity.ok(MapperUtil.INSTANCE.getActivityDto(activityService.saveActivity(activity)));
    }
    @CrossOrigin
    @PutMapping("/activities/{id}")
    public ResponseEntity updateActivity(@PathVariable("id") Long id,@RequestBody ActivityDto activityDto) {
        System.out.println(activityDto.getHost().getId());
        Lecturer lecturer = lecturerService.findById(activityDto.getHost().getId());
        Activity currentAct = activityService.findById(activityDto.getId());
        currentAct.setDescription(activityDto.getDescription());
        currentAct.setLocation(activityDto.getLocation());
        currentAct.setName(activityDto.getName());

        currentAct.setRegisPeriodStart((activityDto.getRegisPeriodStart()));
        currentAct.setRegisPeriodEnd((activityDto.getRegisPeriodEnd()));
        currentAct.setActivityPeriodStart(activityDto.getActivityPeriodStart());
        currentAct.setActivityPeriodEnd(activityDto.getActivityPeriodEnd());
        return ResponseEntity.ok(MapperUtil.INSTANCE.getActivityDto(activityService.saveActivity(currentAct)));
    }
    @CrossOrigin
    @GetMapping("/activities/{id}/enroll/{stdId}")
    public ResponseEntity enrollActivity(@PathVariable("id") Long id, @PathVariable("stdId") Long stdId) {
        Activity activity = activityService.findById(id);
        Student student = studentService.findById(stdId);
        student.getWaitingActivities().add(activity);
        activity.getWaitingStudents().add(student);
        activityService.saveActivity(activity);
        studentService.saveStudent(student);
        return ResponseEntity.ok(MapperUtil.INSTANCE.getActivityDto(activityService.saveActivity(activity)));
    }

    private Activity filterOutWaitingStudent(Activity activity,Student student) {
        Long id = student.getId();
        for (int i=0;i< activity.getWaitingStudents().size();i++) {
            if (activity.getWaitingStudents().get(i).getId().equals(id)) {
                activity.getWaitingStudents().remove(i);
                return activity;
            }
        }
        return activity;
    }
    private Activity filterOutEnrolledStudent(Activity activity,Student student) {
        Long id = student.getId();
        for (int i=0;i< activity.getEnrolledStudents().size();i++) {
            if (activity.getEnrolledStudents().get(i).getId().equals(id)) {
                activity.getEnrolledStudents().remove(i);
                return activity;
            }
        }
        return activity;
    }
    @CrossOrigin
    @GetMapping("/activities/{id}/moveToEnrolled/{stdId}")
    private ResponseEntity moveStudentToEnrolled(@PathVariable("id") Long id, @PathVariable("stdId") Long stdId)
    {
        Activity activity = activityService.findById(id);
        List<Student> waitingStudents = activity.getWaitingStudents();
        Student stu = new Student();
        for (Student s : waitingStudents ) {
            stu = s;
            if (stu.getId().equals( stdId )) {
                for (int i=0; i< stu.getWaitingActivities().size();i++) {
                    if (stu.getWaitingActivities().get(i).getId().equals(id)) {
                        stu.getWaitingActivities().remove(i);
                        activity = filterOutWaitingStudent(activity,stu);
                        activity.getEnrolledStudents().add(stu);
                        activityService.saveActivity(activity);
                        studentService.saveStudent(stu);
                        return ResponseEntity.ok(MapperUtil.INSTANCE.getActivityDto(activityService.saveActivity(activity)));
                    }
                }
            }
        }
        return ResponseEntity.ok(MapperUtil.INSTANCE.getActivityDto(activityService.saveActivity(activity)));

    }
    @CrossOrigin
    @GetMapping("/activities/{id}/withdraw/{stdId}")
    public ResponseEntity withdrawActivity(@PathVariable("id") Long id, @PathVariable("stdId") Long stdId) {
        Activity activity = activityService.findById(id);
        //Student student = studentService.findById(stdId);
        //student.getWaitingActivities().remove(activity);
        List<Student> waitingStudents = activity.getWaitingStudents();
        List<Student> enrolledStudents = activity.getEnrolledStudents();
        Student stu = new Student();
        for (Student s : waitingStudents ) {
            stu = s;
            if (stu.getId().equals( stdId )) {
                for (int i=0; i< stu.getWaitingActivities().size();i++) {
                    if (stu.getWaitingActivities().get(i).getId().equals(id)) {
                        stu.getWaitingActivities().remove(i);
                        activity = filterOutWaitingStudent(activity,stu);
                        activityService.saveActivity(activity);
                        studentService.saveStudent(stu);
                        return ResponseEntity.ok(MapperUtil.INSTANCE.getActivityDto(activityService.saveActivity(activity)));
                    }
                }

            }
        }
        for (Student s : enrolledStudents ) {
            stu = s;
            if (stu.getId().equals( stdId )) {
                for (int i=0; i< stu.getEnrolledActivities().size();i++) {
                    if (stu.getEnrolledActivities().get(i).getId().equals(id)) {
                        stu.getEnrolledActivities().remove(i);
                        activity = filterOutEnrolledStudent(activity,stu);
                        activityService.saveActivity(activity);
                        studentService.saveStudent(stu);
                        return ResponseEntity.ok(MapperUtil.INSTANCE.getActivityDto(activityService.saveActivity(activity)));
                    }
                }

            }
        }
        activityService.saveActivity(activity);
        studentService.saveStudent(stu);
        return ResponseEntity.ok(MapperUtil.INSTANCE.getActivityDto(activityService.saveActivity(activity)));
    }
    @CrossOrigin
    @GetMapping("/activities/lecturer/{id}")
    public ResponseEntity getActivitiesByLecturerId(@PathVariable Long id) {
        List<ActivityDto> activityDtoList = MapperUtil.INSTANCE.getActivityDto(activityService.getAllActivities());
        List<ActivityDto> filteredActivityDtoList = new ArrayList<>();
        for (ActivityDto a : activityDtoList) {
            System.out.println(a.getHost().getId());
            System.out.println(id);
            if (a.getHost().getId() == id) {
                filteredActivityDtoList.add(a);
            }
        }
        return ResponseEntity.ok(filteredActivityDtoList);
    }

    //TODO: MIGHT NEED TO ADD THE MAPPING FOR BOTH LECTURER AND THE ACTIVITY TOO BUT LET TRY FIRST SEE IF THIS WORKS.
}
