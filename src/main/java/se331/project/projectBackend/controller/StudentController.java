package se331.project.projectBackend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import se331.project.projectBackend.dto.StudentUserDto;
import se331.project.projectBackend.entity.Student;
import se331.project.projectBackend.security.entity.User;
import se331.project.projectBackend.security.repository.UserRepository;
import se331.project.projectBackend.service.StudentService;
import se331.project.projectBackend.util.MapperUtil;

@Controller
public class StudentController {
    @Autowired
    StudentService studentService;
    @Autowired
    UserRepository userRepository;
    PasswordEncoder encoder = new BCryptPasswordEncoder();
    @GetMapping("/students")
    public ResponseEntity getAllStudent() {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getStudentDto(studentService.getAllStudent()));
    }
    @GetMapping("/students/{id}")
    public ResponseEntity getStudentById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getStudentDto(studentService.findById(id)));
    }
    @GetMapping("/students/activities/{id}")
    public ResponseEntity getStudentActivitiesById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getStudentActivityDto(studentService.findById(id)));
    }

    @GetMapping("/students/profile/{id}")
    public ResponseEntity getStudentProfileById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getStudentProfileDto(studentService.findById(id)));
    }
    @CrossOrigin
    @PutMapping("/students/profile/{id}")
    public ResponseEntity<?>  updateStudentProfileById(@RequestBody StudentUserDto studentUserDto, @PathVariable Long id) {
        Student student = studentService.findById(id);
        student.setDob(studentUserDto.getDob());
        student.setImage(studentUserDto.getImage());
        student.setName(studentUserDto.getName());
        student.setSurname(studentUserDto.getSurname());
        User user = student.getUser();
        //Set new password
        user.setPassword(encoder.encode(studentUserDto.getPassword()));
        user.setFirstname(studentUserDto.getName());
        user.setLastname(studentUserDto.getSurname());
        student.setUser(user);
        user.setAppUser(student);
        studentService.saveStudent(student);
        userRepository.save(user);
        return ResponseEntity.ok(MapperUtil.INSTANCE.getStudentProfileDto(studentService.findById(id)));
    }

    @DeleteMapping("/students/{id}")
    public ResponseEntity<?> deleteStudentById(@PathVariable long id) {
        return ResponseEntity.ok(studentService.deleteById(id));
    }

    @PostMapping("/students")
    public ResponseEntity saveStudent(@RequestBody Student student) {
        return ResponseEntity.ok(studentService.saveStudent(student));
    }
}
