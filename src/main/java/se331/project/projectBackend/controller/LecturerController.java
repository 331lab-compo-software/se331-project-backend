package se331.project.projectBackend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import se331.project.projectBackend.entity.Comment;
import se331.project.projectBackend.entity.Lecturer;
import se331.project.projectBackend.service.LecturerService;
import se331.project.projectBackend.util.MapperUtil;

@Controller
public class LecturerController {
    @Autowired
    LecturerService lecturerService;

    @GetMapping("/lecturers")
    public ResponseEntity getAllLecturer() {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getLecturerDto(lecturerService.getAllLecturers()));
    }
    @GetMapping("/lecturers/{id}")
    public ResponseEntity getLecturerById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getLecturerDto(lecturerService.findById(id)));
    }
    @DeleteMapping("/lecturers/{id}")
    public ResponseEntity<?> deleteLecturerById(@PathVariable long id) {
        return ResponseEntity.ok(lecturerService.deleteById(id));
    }
    @PostMapping("/lecturers")
    public ResponseEntity saveComment(@RequestBody Lecturer lecturer) {
        return ResponseEntity.ok(lecturerService.saveLecturer(lecturer));
    }
}
