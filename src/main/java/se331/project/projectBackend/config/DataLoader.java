package se331.project.projectBackend.config;

import com.google.api.client.util.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import se331.project.projectBackend.entity.*;
import se331.project.projectBackend.repository.*;
import se331.project.projectBackend.security.entity.Authority;
import se331.project.projectBackend.security.entity.AuthorityName;
import se331.project.projectBackend.security.entity.User;
import se331.project.projectBackend.security.repository.AuthorityRepository;
import se331.project.projectBackend.security.repository.UserRepository;

import javax.transaction.Transactional;
import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;

@Component
public class DataLoader implements ApplicationRunner {
    @Autowired
    StudentRepository studentRepository;
    @Autowired
    LecturerRepository lecturerRepository;
    @Autowired
    ActivityRepository activityRepository;
    @Autowired
    CommentRepository commentRepository;
    @Autowired
    AuthorityRepository authorityRepository;
    @Autowired
    AdminRepository adminRepository;
    @Autowired
    UserRepository userRepository;
    Calendar.Builder calenderBuilder = new Calendar.Builder();

    @Transactional
    @Override
    public void run(ApplicationArguments args) throws Exception {
        //STUDENT
        Student student1 = Student.builder()
                            .studentId("123456789")
                            .name("Okita")
                            .surname("Souji")
                            .image("https://www.googleapis.com/download/storage/v1/b/imageuploader-fc55d.appspot.com/o/2562-11-20%20100506498-1565689999352.jpg?generation=1574219120858573&alt=media")
                            .dob(new Date(1998,4,4))
                            .approved(true)
                            .build();
        //LECTURER
        Lecturer lecturer1 = Lecturer.builder()
                .name("Chartchai")
                .surname("Doungsa-ard")
                .approved(true)
                .image("https://www.googleapis.com/download/storage/v1/b/imageuploader-fc55d.appspot.com/o/2562-12-04%20193939698-tom_cat_shrugging.png?generation=1575463181371199&alt=media")
                .build();
        Lecturer lecturer2 = Lecturer.builder()
                .name("Jayakrit")
                .surname("Hirisajja")
                .approved(true)
                .image("https://www.googleapis.com/download/storage/v1/b/imageuploader-fc55d.appspot.com/o/2562-12-04%20194132490-1558397547483.png?generation=1575463293235404&alt=media")
                .build();
        //ADMIN
        Admin admin = Admin.builder()
                .name("Admin.")
                .surname("Dto")
                .approved(true)
                .build();
        //ACTIVITY
        Activity activity1 = Activity.builder()
                .name("CAMT Eating Contest")
                .location("CAMT 113")
                .description("Let invite CAMT students to eat")
                .host(lecturer1)
                .regisPeriodStart(calenderBuilder.setDate(2019,11,8).build().getTime())
                .regisPeriodEnd(calenderBuilder.setDate(2019,11,22).build().getTime())
                .activityPeriodStart(calenderBuilder.setDate(2019,11,24).setTimeOfDay(9,0,0).build().getTime())
                .activityPeriodEnd(calenderBuilder.setDate(2019,11,24).setTimeOfDay(15,0,0).build().getTime())
                .build();
        Activity activity2 = Activity.builder()
                .name("CAMT Battle Royal Contest")
                .location("CAMT Smo")
                .description("Let battle royal to death")
                .host(lecturer2)
                .regisPeriodStart(calenderBuilder.setDate(2020,0,1).build().getTime())
                .regisPeriodEnd(calenderBuilder.setDate(2020,0,21).build().getTime())
                .activityPeriodStart(calenderBuilder.setDate(2020,1,28).setTimeOfDay(6,0,0).build().getTime())
                .activityPeriodEnd(calenderBuilder.setDate(2020,1,28).setTimeOfDay(20,0,0).build().getTime())
                .build();
        lecturer1.getActivities().add(activity1);
        lecturer2.getActivities().add(activity2);
        activity1 = activityRepository.save(activity1);
        activity2 = activityRepository.save(activity2);
        //AUTHORITY
        Authority authStudent, authLecturer, authAdmin;
        authStudent = Authority.builder().name(AuthorityName.ROLE_STUDENT).build();
        authLecturer = Authority.builder().name(AuthorityName.ROLE_LECTURER).build();
        authAdmin = Authority.builder().name(AuthorityName.ROLE_ADMIN).build();
        authStudent = authorityRepository.save(authStudent);
        authLecturer = authorityRepository.save(authLecturer);
        authAdmin = authorityRepository.save(authAdmin);
        //USER
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        User adminUser, studentUser1,lecturerUser1, lecturerUser2;
        adminUser = User.builder()
                .username("admin@admin.com")
                .password(encoder.encode("admin"))
                .firstname("Admin")
                .lastname("Admin")
                .email("admin@admin.com")
                .enabled(true)
                .lastPasswordResetDate(Date.from(LocalDate.of(2019,01,01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
                .build();
        adminUser.getAuthorities().add(authAdmin);
        studentUser1 = User.builder()
                .username("student1@elearning.cmu.ac.th")
                .password(encoder.encode("student1password"))
                .firstname("Okita")
                .lastname("Souji")
                .email("student1@elearning.cmu.ac.th")
                .enabled(true)
                .lastPasswordResetDate(Date.from(LocalDate.of(2019,01,01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
                .build();
        studentUser1.getAuthorities().add(authStudent);
        lecturerUser1 = User.builder()
                .username("teacher1@elearning.cmu.ac.th")
                .password(encoder.encode("teacher1password"))
                .firstname("Chartchai")
                .lastname("Doungsa-ard")
                .email("teacher1@elearning.cmu.ac.th")
                .enabled(true)
                .lastPasswordResetDate(Date.from(LocalDate.of(2019,01,01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
                .build();
        lecturerUser2 = User.builder()
                .username("teacher2@elearning.cmu.ac.th")
                .password(encoder.encode("teacher2password"))
                .firstname("Jayakrit")
                .lastname("Hirisajja")
                .email("teacher2@elearning.cmu.ac.th")
                .enabled(true)
                .lastPasswordResetDate(Date.from(LocalDate.of(2019,01,01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
                .build();
        lecturerUser1.getAuthorities().add(authLecturer);
        lecturerUser2.getAuthorities().add(authLecturer);
        //COMMENT
        Comment comment1 = Comment.builder()
                .image("")
                .text("俺の名前は、沖田総司だ")
                .time(calenderBuilder.setDate(2019,11,25).build().getTime())
                .activity(activity1)
                .poster(studentUser1)
                .build();
        Comment comment2 = Comment.builder()
                .image("")
                .text("Get me back to the Shinsengumi!!!")
                .time(calenderBuilder.setDate(2019,11,26).build().getTime())
                .activity(activity1)
                .poster(studentUser1)
                .build();
        comment1 = commentRepository.save(comment1);
        comment2 = commentRepository.save(comment2);
        activity1.getComments().add(comment1);
        activity1.getComments().add(comment2);
        //
        activity1 = activityRepository.save(activity1);
        activity2 = activityRepository.save(activity2);
        //
        student1.setUser(studentUser1);
        studentUser1.setAppUser(student1);
        admin.setUser(adminUser);
        adminUser.setAppUser(admin);
        lecturer1.setUser(lecturerUser1);
        lecturerUser1.setAppUser(lecturer1);
        lecturer2.setUser(lecturerUser2);
        lecturerUser2.setAppUser(lecturer2);
        //
        studentUser1 = userRepository.save( studentUser1);
        adminUser = userRepository.save(adminUser );
        lecturerUser1 = userRepository.save(lecturerUser1);
        lecturerUser2 = userRepository.save(lecturerUser2);
        student1 = studentRepository.save(student1);
        admin = adminRepository.save(admin);
        lecturer1 = lecturerRepository.save(lecturer1);
        lecturer2 = lecturerRepository.save(lecturer2);
    }


}
