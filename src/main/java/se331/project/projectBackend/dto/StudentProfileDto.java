package se331.project.projectBackend.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StudentProfileDto {
    Long id;
    String studentId;
    String name;
    String surname;
    String image;
    String password;
    String email;
    @JsonFormat(pattern="yyyy-MM-dd")
    Date dob;
}
