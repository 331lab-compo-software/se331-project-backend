package se331.project.projectBackend.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import se331.project.projectBackend.entity.Comment;
import se331.project.projectBackend.entity.Lecturer;
import se331.project.projectBackend.entity.Student;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ActivityDto {
    Long id;
    String name;
    String location;
    String description;
    LecturerDto host;
    @JsonFormat(pattern="yyyy-MM-dd")
    Date regisPeriodStart;
    @JsonFormat(pattern="yyyy-MM-dd")
    Date regisPeriodEnd;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    Date activityPeriodStart;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    Date activityPeriodEnd;
    @Builder.Default
    List<CommentDto> comments = new ArrayList<>();
    @Builder.Default
    List<StudentDto> waitingStudents = new ArrayList<>();
    @Builder.Default
    List<StudentDto> enrolledStudents = new ArrayList<>();

}
