package se331.project.projectBackend.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import se331.project.projectBackend.security.entity.User;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CommentDto {
    Long id;
    String image;
    String text;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    Date time;
    UserDto poster;
}
