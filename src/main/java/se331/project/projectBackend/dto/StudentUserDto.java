package se331.project.projectBackend.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import se331.project.projectBackend.security.entity.Authority;
import se331.project.projectBackend.security.entity.User;

import java.util.Date;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StudentUserDto {
    Long id;
    String studentId;
    String username;
    String name;
    String surname;
    @JsonFormat(pattern="yyyy-MM-dd")
    Date dob;
    String image;
    String password;
    String email;
    boolean approved;
    Date lastPasswordResetDate;
    List<Authority> authorities;
    User user;


}
