package se331.project.projectBackend.dto;

import lombok.*;
import se331.project.projectBackend.entity.Activity;

import javax.persistence.ManyToMany;
import java.util.ArrayList;
import java.util.List;
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StudentActivityDto {
    Long id;
    String studentId;
    String name;
    String surname;
    String image;
    List<ActivityDto> waitingActivities = new ArrayList<>();
    List<ActivityDto> enrolledActivities = new ArrayList<>();
}
