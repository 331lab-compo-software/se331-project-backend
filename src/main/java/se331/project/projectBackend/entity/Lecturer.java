package se331.project.projectBackend.entity;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class Lecturer extends Person{
    @OneToMany(mappedBy = "host")
    @Builder.Default
    @ToString.Exclude
    List<Activity> activities = new ArrayList<>();

}
