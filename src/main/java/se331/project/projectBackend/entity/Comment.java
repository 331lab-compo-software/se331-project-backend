package se331.project.projectBackend.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import se331.project.projectBackend.security.entity.User;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@SuperBuilder
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@NoArgsConstructor
@AllArgsConstructor
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @EqualsAndHashCode.Exclude
    Long id;
    String image;
    String text;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    Date time;
    @ManyToOne
    @JsonBackReference(value="activityReference")
    Activity activity;
    @ManyToOne
    User poster;


}
