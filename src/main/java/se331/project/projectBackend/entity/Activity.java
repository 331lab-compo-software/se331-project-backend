package se331.project.projectBackend.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Activity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @EqualsAndHashCode.Exclude
    Long id;
    String name;
    String location;
    String description;
    @ManyToOne
    @JsonBackReference
    Lecturer host;
    @OneToMany(mappedBy="activity")
    @Builder.Default

    @ToString.Exclude
    List<Comment> comments = new ArrayList<>();
    @JsonFormat(pattern="yyyy-MM-dd")
    Date regisPeriodStart;
    @JsonFormat(pattern="yyyy-MM-dd")
    Date regisPeriodEnd;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    Date activityPeriodStart;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    Date activityPeriodEnd;
    @ManyToMany
    @Builder.Default
    @JsonBackReference(value="waitingStudentsReference")
    @ToString.Exclude
    List<Student> waitingStudents = new ArrayList<>();
    @ManyToMany
    @Builder.Default
    @JsonBackReference(value="enrolledStudentsReference")
    @ToString.Exclude
    List<Student> enrolledStudents = new ArrayList<>();

}
