package se331.project.projectBackend.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class Student extends Person {
    String studentId;
    @JsonFormat(pattern="yyyy-MM-dd")
    Date dob;

    @ManyToMany(mappedBy = "waitingStudents")
    @Builder.Default
    @ToString.Exclude
    List<Activity> waitingActivities = new ArrayList<>();
    @ManyToMany(mappedBy = "enrolledStudents")
    @Builder.Default
    @ToString.Exclude
    List<Activity> enrolledActivities = new ArrayList<>();


}
