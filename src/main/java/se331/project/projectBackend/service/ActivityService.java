package se331.project.projectBackend.service;

import se331.project.projectBackend.entity.Activity;

import java.util.List;

public interface ActivityService {
    List<Activity> getAllActivities();
    Activity findById(Long id);
    Activity deleteById(Long id);
    Activity saveActivity(Activity activity);
}
