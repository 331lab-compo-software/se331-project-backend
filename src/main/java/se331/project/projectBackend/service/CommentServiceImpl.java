package se331.project.projectBackend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se331.project.projectBackend.dao.CommentDao;
import se331.project.projectBackend.entity.Comment;

import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {
    @Autowired
    CommentDao commentDao;
    @Override
    public List<Comment> getAllComments() {
        return commentDao.getAllComments();
    }

    @Override
    public Comment findById(Long id) {
        return commentDao.findById(id);
    }

    @Override
    public Comment deleteById(Long id) {
        return commentDao.deleteById(id);
    }

    @Override
    public Comment saveComment(Comment comment) {
        return commentDao.saveComment(comment);
    }
}
