package se331.project.projectBackend.service;

import se331.project.projectBackend.entity.Comment;

import java.util.List;

public interface CommentService {
    List<Comment> getAllComments();
    Comment findById(Long id);
    Comment deleteById(Long id);
    Comment saveComment(Comment comment);
}
