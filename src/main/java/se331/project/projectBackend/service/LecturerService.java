package se331.project.projectBackend.service;

import se331.project.projectBackend.entity.Lecturer;

import java.util.List;

public interface LecturerService {
    List<Lecturer> getAllLecturers();
    Lecturer findById(Long id);
    Lecturer deleteById(Long id);
    Lecturer saveLecturer(Lecturer lecturer);
}
