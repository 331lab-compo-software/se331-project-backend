package se331.project.projectBackend.service;

import se331.project.projectBackend.entity.Student;

import java.util.List;

public interface StudentService {
    List<Student> getAllStudent();
    Student findById(Long id);
    Student deleteById(Long id);
    Student saveStudent(Student student);
}
