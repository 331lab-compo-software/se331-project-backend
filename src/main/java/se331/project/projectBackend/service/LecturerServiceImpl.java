package se331.project.projectBackend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se331.project.projectBackend.dao.LecturerDao;
import se331.project.projectBackend.entity.Lecturer;

import java.util.List;

@Service
public class LecturerServiceImpl implements LecturerService {
    @Autowired
    LecturerDao lecturerDao;
    @Override
    public List<Lecturer> getAllLecturers() {
        return lecturerDao.getAllLecturers();
    }

    @Override
    public Lecturer findById(Long id) {
        return lecturerDao.findById(id);
    }

    @Override
    public Lecturer deleteById(Long id) {
        return lecturerDao.deleteById(id);
    }

    @Override
    public Lecturer saveLecturer(Lecturer lecturer) {
        return lecturerDao.saveLecturer(lecturer);
    }
}
