package se331.project.projectBackend.security.repository;

import org.springframework.data.repository.CrudRepository;
import se331.project.projectBackend.security.entity.Authority;
import se331.project.projectBackend.security.entity.AuthorityName;

public interface AuthorityRepository extends CrudRepository<Authority,Long> {
    Authority findByName(AuthorityName input);
}
