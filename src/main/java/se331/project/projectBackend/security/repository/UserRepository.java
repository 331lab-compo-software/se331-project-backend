package se331.project.projectBackend.security.repository;

import org.springframework.data.repository.CrudRepository;
import se331.project.projectBackend.security.entity.User;

import java.util.List;

public interface UserRepository extends CrudRepository<User,Long> {
    User findByUsername(String username);
    User findByEmail(String email);
    User findByAppUserId(Long id);
    List<User> findAll();
}
