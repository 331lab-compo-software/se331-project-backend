package se331.project.projectBackend.security.entity;

public enum AuthorityName {
    ROLE_STUDENT,ROLE_LECTURER,ROLE_ADMIN
}