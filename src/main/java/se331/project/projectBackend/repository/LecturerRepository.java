package se331.project.projectBackend.repository;

import org.springframework.data.repository.CrudRepository;
import se331.project.projectBackend.entity.Lecturer;

import java.util.List;

public interface LecturerRepository extends CrudRepository<Lecturer,Long> {
    List<Lecturer> findAll();
    List<Lecturer> findByNameIgnoreCaseContaining(String name);
    List<Lecturer> findBySurnameIgnoreCaseContaining(String name);

}
