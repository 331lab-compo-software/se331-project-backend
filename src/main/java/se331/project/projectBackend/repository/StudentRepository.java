package se331.project.projectBackend.repository;

import org.springframework.data.repository.CrudRepository;
import se331.project.projectBackend.entity.Student;

import java.util.List;

public interface StudentRepository extends CrudRepository<Student,Long>
{
    List<Student> findAll();
    List<Student> findByEnrolledActivitiesId(Long id);
    List<Student> findByWaitingActivitiesId(Long id);
}
