package se331.project.projectBackend.repository;

import org.springframework.data.repository.CrudRepository;
import se331.project.projectBackend.entity.Activity;

import java.util.List;

public interface ActivityRepository extends CrudRepository<Activity,Long> {
    List<Activity> findAll();
    List<Activity> findByNameIgnoreCaseContaining(String partOfName);
    Activity findByEnrolledStudentsGreaterThanEqual(Integer number);
    Activity findByWaitingStudentsGreaterThanEqual(Integer number);
    List<Activity> findByWaitingStudentsId(Long studentId);
    List<Activity> findByEnrolledStudentsId(Long studentId);


}
