package se331.project.projectBackend.repository;

import org.springframework.data.repository.CrudRepository;
import se331.project.projectBackend.entity.Comment;

import java.util.List;

public interface CommentRepository extends CrudRepository<Comment,Long> {
    List<Comment> findAll();
    List<Comment> findByPosterId(Long userId);
    List<Comment> findByActivityId(Long actId);
}
