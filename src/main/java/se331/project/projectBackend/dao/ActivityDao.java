package se331.project.projectBackend.dao;

import se331.project.projectBackend.entity.Activity;
import se331.project.projectBackend.entity.Lecturer;

import java.util.List;

public interface ActivityDao {
    List<Activity> getAllActivities();
    Activity findById(Long id);
    Activity deleteById(Long id);
    Activity saveActivity(Activity activity);
}
