package se331.project.projectBackend.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import se331.project.projectBackend.entity.Activity;
import se331.project.projectBackend.repository.ActivityRepository;

import java.util.List;

@Repository
public class ActivityDaoImpl implements ActivityDao {
    @Autowired
    ActivityRepository activityRepository;

    @Override
    public List<Activity> getAllActivities() {
        return activityRepository.findAll();
    }
    
    @Override
    public Activity findById(Long id) {
        return activityRepository.findById(id).orElse(null);
    }

    @Override
    public Activity deleteById(Long id) {
        Activity activity = activityRepository.findById(id).orElse(null);
        activityRepository.deleteById(id);
        return activity;
    }

    @Override
    public Activity saveActivity(Activity activity) {
        return activityRepository.save(activity);
    }
}
