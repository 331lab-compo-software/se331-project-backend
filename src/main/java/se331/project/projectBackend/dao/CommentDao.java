package se331.project.projectBackend.dao;

import se331.project.projectBackend.entity.Activity;
import se331.project.projectBackend.entity.Comment;

import java.util.List;

public interface CommentDao {
    List<Comment> getAllComments();
    Comment findById(Long id);
    Comment deleteById(Long id);
    Comment saveComment(Comment comment);
}
