package se331.project.projectBackend.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import se331.project.projectBackend.entity.Lecturer;
import se331.project.projectBackend.entity.Student;
import se331.project.projectBackend.repository.StudentRepository;

import java.util.List;

@Repository
public class StudentDaoImpl implements StudentDao {
    @Autowired
    StudentRepository studentRepository;

    @Override
    public List<Student> getAllStudent() {
        return studentRepository.findAll();
    }

    @Override
    public Student findById(Long id) {
        return studentRepository.findById(id).orElse(null);
    }

    @Override
    public Student deleteById(Long id) {
        Student student = studentRepository.findById(id).orElse(null);
        studentRepository.deleteById(id);
        return student;
    }

    @Override
    public Student saveStudent(Student student) {
        return studentRepository.save(student);
    }
}
