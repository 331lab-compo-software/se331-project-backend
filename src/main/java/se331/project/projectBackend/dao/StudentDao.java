package se331.project.projectBackend.dao;

import se331.project.projectBackend.entity.Student;

import java.util.List;

public interface StudentDao {
    List<Student> getAllStudent();
    Student findById(Long id);
    Student deleteById(Long id);
    Student saveStudent(Student student);

}
