package se331.project.projectBackend.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import se331.project.projectBackend.entity.Comment;
import se331.project.projectBackend.entity.Lecturer;
import se331.project.projectBackend.repository.LecturerRepository;

import java.util.List;
@Repository
public class LecturerDaoImpl implements LecturerDao {
    @Autowired
    LecturerRepository lecturerRepository;

    @Override
    public List<Lecturer> getAllLecturers() {
        return lecturerRepository.findAll();
    }

    @Override
    public Lecturer findById(Long id) {
        return lecturerRepository.findById(id).orElse(null);
    }

    @Override
    public Lecturer deleteById(Long id) {
        Lecturer lecturer = lecturerRepository.findById(id).orElse(null);
        lecturerRepository.deleteById(id);
        return  lecturer;
    }

    @Override
    public Lecturer saveLecturer(Lecturer lecturer) {
        return lecturerRepository.save(lecturer);
    }
}
