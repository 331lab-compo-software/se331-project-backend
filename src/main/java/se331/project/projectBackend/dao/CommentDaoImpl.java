package se331.project.projectBackend.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import se331.project.projectBackend.entity.Activity;
import se331.project.projectBackend.entity.Comment;
import se331.project.projectBackend.repository.CommentRepository;

import java.util.List;

@Repository
public class CommentDaoImpl implements CommentDao {
    @Autowired
    CommentRepository commentRepository;

    @Override
    public List<Comment> getAllComments() {
        return commentRepository.findAll();
    }

    @Override
    public Comment findById(Long id) {
        return commentRepository.findById(id).orElse(null);
    }

    @Override
    public Comment deleteById(Long id) {
        Comment comment =  commentRepository.findById(id).orElse(null);
        commentRepository.deleteById(id);
        return  comment;
    }

    @Override
    public Comment saveComment(Comment comment) {
        return commentRepository.save(comment);
    }
}
