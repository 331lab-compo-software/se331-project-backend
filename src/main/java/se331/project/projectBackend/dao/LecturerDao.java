package se331.project.projectBackend.dao;

import se331.project.projectBackend.entity.Lecturer;
import se331.project.projectBackend.entity.Student;

import java.util.List;

public interface LecturerDao {
    List<Lecturer> getAllLecturers();
    Lecturer findById(Long id);
   Lecturer deleteById(Long id);
    Lecturer saveLecturer(Lecturer lecturer);

}
