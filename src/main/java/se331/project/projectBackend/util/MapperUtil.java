package se331.project.projectBackend.util;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;
import se331.project.projectBackend.dto.*;
import se331.project.projectBackend.entity.*;
import se331.project.projectBackend.security.entity.User;

import java.util.List;

@Mapper
public interface MapperUtil {
    MapperUtil INSTANCE = Mappers.getMapper( MapperUtil.class );

    @Mappings({})
    LecturerDto getLecturerDto(Lecturer lecturer);

    @Mappings({})
    List<LecturerDto> getLecturerDto(List<Lecturer> lecturers);

    //TODO: CONSIDER DELETE THIS
    @Mappings({})
    AdminDto getAdminDto(Admin admin);
    //TODO: CONSIDER DELETE THIS
    @Mappings({})
    List<AdminDto> getAdminDto(List<Admin> admins);

    @Mappings({})
    StudentDto getStudentDto(Student student);

    @Mappings({})
    List<StudentDto> getStudentDto(List<Student> students);

    @Mappings({})
    ActivityDto getActivityDto(Activity activity);

    @Mappings({})
    List<ActivityDto> getActivityDto(List<Activity> activities);

    @Mappings({})
    CommentDto getCommentDto(Comment comment);

    @Mappings({})
    List<CommentDto> getCommentDto(List<Comment> comment);

    @Mappings({
            @Mapping(target = "authorities", source = "user.authorities")
    })
    UserDto getUserDto(Student student);
    @Mappings({
            @Mapping(target = "authorities", source = "user.authorities")
    })
    UserDto getUserDto(Lecturer lecturer);
    //TODO: CONSIDER DELETE THIS
    @Mappings({
            @Mapping(target = "authorities", source = "user.authorities")
    })
    UserDto getUserDto(Admin admin);
    @Mappings({
            @Mapping(target = "password", source = "user.password"),
            @Mapping(target = "email", source = "user.username"),
    })
    StudentProfileDto getStudentProfileDto(Student student);
    @Mappings({

    })
    StudentActivityDto getStudentActivityDto(Student student);

    @Mappings({
            @Mapping(target = "authorities", source = "user.authorities"),
            @Mapping(target = "name", source = "user.appUser.name"),
            @Mapping(target = "surname", source = "user.appUser.surname"),
            @Mapping(target = "image", source = "user.appUser.image")
    })
    UserDto getUserDto(User user);
}
